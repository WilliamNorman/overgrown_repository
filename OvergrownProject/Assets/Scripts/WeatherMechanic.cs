﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeatherMechanic : MonoBehaviour {

    float weatherTimer;
    int weatherType;
    public GameObject rain, sunshine;
	// Use this for initialization
	void Start () {

        weatherTimer = Random.Range(30, 90); 
    }
	
	// Update is called once per frame
	void Update () {

        weatherTimer -= Time.deltaTime;
        if(weatherTimer <= 0)
        {
           weatherType = Random.Range(1, 2);

            switch (weatherType)
            {
                case 1:
                    rain.SetActive(true);
                    break;

                case 2:
                    sunshine.SetActive(true);
                    break;

                default:
                    rain.SetActive(true); 
                    break;
            }

            weatherTimer = Random.Range(30, 90);

        }
		
	}
}
