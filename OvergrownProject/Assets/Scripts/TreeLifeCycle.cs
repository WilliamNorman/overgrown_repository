﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeLifeCycle : MonoBehaviour {

    public GameObject stump;   

    public void ChopDownTree()
    {
        for (int i = 0; i < 3; i ++) //create 3 wooden blocks, destory self
        {
            Instantiate(Resources.Load<GameObject>("WoodenBlock"), new Vector3(transform.position.x, 1.5f, transform.position.z), Quaternion.identity);
        }
        Destroy(gameObject);
        Instantiate(stump,transform.position,Quaternion.identity);
        
    }     

}
