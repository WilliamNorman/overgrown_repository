﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectThrownAtBin : MonoBehaviour {

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Item")
        {
            if(other.GetComponentInChildren<PickUpPhysics>()._beingPickedUp == false && other.transform.root.gameObject.tag != "Player")
            {    
                Destroy(other.transform.root.gameObject);                
            }
            
        }
    }

}
