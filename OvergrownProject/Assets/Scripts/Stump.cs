﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stump : MonoBehaviour {	

    public void DigUpStump()
    {
        for (int i = 0; i < 2; i++)//create 2 wooden blocks then destory self
        {
            Instantiate(Resources.Load<GameObject>("WoodenBlock"), new Vector3(transform.position.x, 1.5f, transform.position.z), Quaternion.identity);
        }
        Destroy(gameObject);
    }
}
