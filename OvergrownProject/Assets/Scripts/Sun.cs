﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sun : MonoBehaviour {

    float lifeTimer = 15;

    // Use this for initialization
    void Start()
    {


    }


    // Update is called once per frame
    void Update()
    {
        WeedSpawner.WeedTimerScale = 1.5f;
        lifeTimer -= Time.deltaTime;

        if (lifeTimer <= 0)
        {
            WeedSpawner.WeedTimerScale = 1;
            gameObject.SetActive(false);
            lifeTimer = 15;
        }

    }
}

