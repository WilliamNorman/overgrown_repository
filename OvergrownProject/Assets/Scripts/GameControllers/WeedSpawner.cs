﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Written by William Norman
public class WeedSpawner : MonoBehaviour {

    public static float WeedTimerScale = 1;

    // Counters for the soil tile statuses 
    [HideInInspector]
    public int EmptySoilCount;
    [HideInInspector]
    public int WeedSoilCount;
    [HideInInspector]
    public int OccupiedSoilCount;

    [SerializeField]
    string _soilTag = "Soil";
    [SerializeField]
    float _weedSpawnTime = 10f;// time between weed spawning

    [HideInInspector]
    public List <SoilLifecycle> _soilScriptList = new List<SoilLifecycle>();// All the SoilLifeCycle scripts are stored here 

    void Awake () {
        // Stores all soil scripts in the list and starts the timer
        GameObject[] _soilTileArray = GameObject.FindGameObjectsWithTag(_soilTag);
        for(int i = 0; i < _soilTileArray.Length; i++)
        {
            _soilScriptList.Add(_soilTileArray[i].GetComponent<SoilLifecycle>());
        }
        StartCoroutine(WeedSpawnTimer());
    }

    IEnumerator WeedSpawnTimer()
    {
        yield return new WaitForSeconds(_weedSpawnTime * WeedTimerScale);
        SpawnWeed();
    }

    void SpawnWeed()
    {
        // Create empty list and reset counters
        List<SoilLifecycle> _emptySoilList = new List<SoilLifecycle>();// The tiles weed can be spawned on
        EmptySoilCount = 0; WeedSoilCount = 0; OccupiedSoilCount = 0;

        // Loops through list and stores empty soilTiles
        for (int i = 0; i < _soilScriptList.Count; i++)
        {
            if (_soilScriptList[i].CurrentState == SoilStates.EmptySoil)
            {
                _emptySoilList.Add(_soilScriptList[i]);
                EmptySoilCount++;
            }
            else if (_soilScriptList[i].CurrentState == SoilStates.SmallWeed || _soilScriptList[i].CurrentState == SoilStates.LargeWeed)
            {
                WeedSoilCount++;
            }
            else
            {
                OccupiedSoilCount++;
            }
        }

        if(_emptySoilList.Count >= 1)// If there 1 or more empty tiles then it'll spawn a weed on a random one
        {
            int randomNum = Random.Range(0, _emptySoilList.Count);
            _emptySoilList[randomNum].SpawnSmallWeed();
        }
        StartCoroutine(WeedSpawnTimer());// Reset timer
    }    
}
