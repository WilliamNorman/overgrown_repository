﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicController : MonoBehaviour {

	void Awake()
    {
        float timeToWait = FindObjectOfType<Timer>().timeRemaining * 0.7f;
        Invoke("IncreaseMusicTempo", timeToWait);
    }

    public void PlayMusic()
    {
        FindObjectOfType<_AudioManager>().PlayAudioClip(GetComponents<AudioSource>()[1], "HustleNormal", true);
    }

    void IncreaseMusicTempo()
    {
        FindObjectOfType<_AudioManager>().PlayAudioClip(GetComponents<AudioSource>()[1], "HustleFast", true);
    }
}
