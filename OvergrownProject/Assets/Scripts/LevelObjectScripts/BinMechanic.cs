﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BinMechanic : MonoBehaviour
{

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            ObjectPickUp objectPickUp = other.gameObject.GetComponent<ObjectPickUp>();

            if (objectPickUp._holdingObject == true)// checks to see if the player is holding an object
            {
                if (Input.GetButtonDown("UseTool"))
                {
                    if (objectPickUp._currentObject.tag == "Item")
                    {
                        Destroy(objectPickUp._currentObject);//if the object is a weed destroy it
                        objectPickUp._currentObject = null;
                        objectPickUp._holdingObject = false;
                    }
                }
            }
        }
    }
}
