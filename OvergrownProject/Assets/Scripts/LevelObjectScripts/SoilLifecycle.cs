﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Written by William Norman
public enum SoilStates { MegaWeed, LargeWeed, SmallWeed, EmptySoil, SeededSoil, DryPlant, WateredPlant }// All the potential states for the soil, inside a public enum

public class SoilLifecycle : MonoBehaviour
{

    public SoilStates CurrentState { get; set; }// This stores the current state 

    [HideInInspector]
    public static float PlantDryingSpeed = 1;// This can be changed to make the game harder/easier
    public bool _isWeedSprayed;// Once weed killer is sprayed then this will be true

    // The time variables that store how long to wait for certain procedures
    [Header("Time variables")]
    [SerializeField]
    private float _smallWeedGrowTime = 10;
    [SerializeField]
    private float _largeWeedGrowTime = 25;
    [SerializeField]
    private float _plantDryingTime = 20;
    [SerializeField]
    private float _seedDecayTime = 6;
    public float _waterLevel;// How much water is stored   
    public bool _fullyWatered = false;
    private GameObject _currentPlant;//The current plant/weed on the soil   
    private LoadingBar _loadingBar;//The loading bar UI

    // All the relevant prefabs
    [SerializeField]
    GameObject smallWeed, largeWeed, megaWeed, seedPrefab, plantPrefab, loadingBarPrefab;
    // The spawnpoint for the prefabs
    private Transform _plantSpawnPoint;

    void Awake()
    {
        _plantSpawnPoint = transform.Find("SpawnPoint");
        CurrentState = SoilStates.EmptySoil;
        //
        _loadingBar = Instantiate(loadingBarPrefab, this.transform.position, Quaternion.identity).GetComponent<LoadingBar>();
        _loadingBar.MoveLoadingBar(this.transform);
    }


    #region Healthy Soil Methods
    public void PlaceSeeds()
    {
        StopAllCoroutines();
        CurrentState = SoilStates.SeededSoil;
        StartCoroutine(SpawnObject(seedPrefab));
        StartCoroutine(SeedDecay());
    }

    //Waters seeds so it can grow into a plant and refills the water of existing plants
    public void WaterSoil(float _wateringSpeed)
    {
        if (CurrentState == SoilStates.WateredPlant)
        {
            //Adds whatever the plant drying coroutine will take away, plus the normal watering value
            _waterLevel += (Time.deltaTime / _plantDryingTime) + (Time.deltaTime * _wateringSpeed);
            if (_waterLevel >= 1)
            {
                _fullyWatered = true;
                Invoke("MakePlantWaterable", 2f);
            }
        }

        if (CurrentState == SoilStates.SeededSoil)
        {
            _waterLevel += Time.deltaTime * _wateringSpeed;
            _loadingBar.SetBarAmount(_waterLevel, 1);
            if (_waterLevel >= 1)
            {
                StopAllCoroutines();
                CurrentState = SoilStates.WateredPlant;
                StartCoroutine(SpawnObject(plantPrefab));
                StartCoroutine(PlantDrying());
                _loadingBar.SetBarAmount(0, 1);

                _fullyWatered = true;
                Invoke("MakePlantWaterable", 2f);
            }
        }       
    }

    private void MakePlantWaterable()
    { _fullyWatered = false; }

    private void RemovePlant()// When a plant dies the soil will empty, 
    {
        CurrentState = SoilStates.EmptySoil;
        StopAllCoroutines();
        StartCoroutine(SpawnObject(null));
    }

    #endregion

    #region Weed Methods
    public void SpawnSmallWeed()// This'll be called by the WeedSpawner,  spawns the small weed and starts WeedGroth coroutine
    {
        CurrentState = SoilStates.SmallWeed;
        StartCoroutine(SpawnObject(smallWeed));
        StartCoroutine(WeedGrowth());
    }

    private void SpawnLargeWeed()// Called automatically after the small weed has grown
    {
        CurrentState = SoilStates.LargeWeed;
        StartCoroutine(SpawnObject(largeWeed));
        StartCoroutine(WeedGrowth());
    }

    private void SpawnMegaWeed()// When the weed reaches its final stage it'll jump out of the soil and leave empty soil behind
    {
        CurrentState = SoilStates.EmptySoil;
        StopAllCoroutines();
        StartCoroutine(SpawnObject(null));
        Instantiate(megaWeed, _plantSpawnPoint.position, _plantSpawnPoint.rotation);
    }

    public GameObject RemoveSmallWeed()// This will return a GameObject to the player so they can immediately pick it up
    {
        CurrentState = SoilStates.EmptySoil;
        StopAllCoroutines();

        Destroy(_currentPlant);
        _currentPlant = null;
        GameObject tempGO = Instantiate(smallWeed, _plantSpawnPoint.position, _plantSpawnPoint.rotation);
        tempGO.tag = "Item";
        return tempGO;
    }

    public void RemoveLargeWeed()// This will require the isWeedSprayed to be true
    {
        //Change weed back into small weed 
        CurrentState = SoilStates.SmallWeed;
        StopAllCoroutines();

        Destroy(_currentPlant);
        _currentPlant = Instantiate(smallWeed, _plantSpawnPoint.position, _plantSpawnPoint.rotation);

        PickUpPhysics pickUpPhysics = Instantiate(smallWeed, _plantSpawnPoint.position, _plantSpawnPoint.rotation).GetComponent<PickUpPhysics>();
        pickUpPhysics.EnablePhysics();
        pickUpPhysics.RB.AddForce(_plantSpawnPoint.forward + _plantSpawnPoint.up * 5f, ForceMode.Impulse);
        pickUpPhysics.gameObject.tag = "Item";

        StartCoroutine(WeedGrowth());
    }

    #endregion

    #region Coroutines
    IEnumerator WeedGrowth()//When the small weed is spawned this'll start. It will run until the next weed stage is activated or the current weed is removed 
    {
        if (CurrentState == SoilStates.SmallWeed)
        {
            yield return new WaitForSeconds(_smallWeedGrowTime);
            SpawnLargeWeed();
        }
        else if (CurrentState == SoilStates.LargeWeed)
        {
            yield return new WaitForSeconds(_largeWeedGrowTime);
            SpawnMegaWeed();
        }
    }

    // Handles the instantiating of new plants and destroys the old one
    // If a null GameObject is sent, it will deestroy the current object and spawn nothing in its place
    IEnumerator SpawnObject(GameObject givenObject)
    {
        float scaleSpeed = 2;
        if (_currentPlant != null)// If there is no plant to scale/destroy then it'll skip this part
        {
            
            while (_plantSpawnPoint.localScale.y >= 0f)// Shrinks object on Y axis
            {
                _plantSpawnPoint.localScale -= new Vector3(0, Time.deltaTime * scaleSpeed, 0);
                yield return null;
            }
            Destroy(_currentPlant);
        }
        else
            _plantSpawnPoint.localScale = new Vector3(1, 0, 1);// prepares spawnpoint for new plant

        if (givenObject != null)
        {
            //This spawns the object, sets it's parent as the spawn point and sets the Y to 1
            _currentPlant = Instantiate(givenObject, _plantSpawnPoint.position, _plantSpawnPoint.rotation);
            _currentPlant.transform.parent = _plantSpawnPoint;
            _currentPlant.transform.localScale = new Vector3(givenObject.transform.localScale.x, 1, givenObject.transform.localScale.y);

            while (_plantSpawnPoint.localScale.y <= 1)// Grows object on Y axis
            {
                _plantSpawnPoint.localScale += new Vector3(0, Time.deltaTime * scaleSpeed, 0);
                yield return null;
            }
        }
        else
            _currentPlant = null;
    }

    IEnumerator PlantDrying()// Runs whilst plant exists. It's reset when the plant is watered again and it will kill the plant if the coroutine is completed
    {
        while (CurrentState == SoilStates.WateredPlant)
        {
            Material plantMat = _currentPlant.GetComponentInChildren<MeshRenderer>().material;
            _waterLevel -= Time.deltaTime / _plantDryingTime * PlantDryingSpeed * Time.timeScale;
            plantMat.color = Color.white * _waterLevel;//((_waterLevel - 1) * -1);
            if (_waterLevel < 0)
            {
                CurrentState = SoilStates.EmptySoil;
                _loadingBar.HideWarning();
            }
            else if(_waterLevel < 0.2f)
            {
                _loadingBar.ShowWarning();
            }
            else
            {
                _loadingBar.HideWarning();
            }
            yield return null;
        }
        RemovePlant();
    }

    //Stops seed from lasting forever
    IEnumerator SeedDecay()
    {
        yield return new WaitForSeconds(_seedDecayTime);
        CurrentState = SoilStates.EmptySoil;
        _loadingBar.SetBarAmount(0, 1);
        StartCoroutine(SpawnObject(null));
    }
    #endregion
}
