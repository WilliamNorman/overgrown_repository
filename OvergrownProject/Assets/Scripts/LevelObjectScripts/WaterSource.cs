﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Written by William Norman
public class WaterSource : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            GameObject currentObject = other.gameObject.GetComponent<ObjectPickUp>().CurrentObject;
            if(currentObject != null && currentObject.GetComponent<WateringCan>() != null)//Is there an object and does it have the relevant script?
            {
                currentObject.GetComponent<WateringCan>().FillWateringCan();
            }

        }
    }

}
