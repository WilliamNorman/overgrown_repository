﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Written by William Norman
public class GrassTile : MonoBehaviour {
    
    public bool _overgrownGrass = false;

    GameObject grassMesh;
    float _grassLevel = 0;

    void Awake()
    {
        //If is bool is set to true then enable grass mesh
        if (_overgrownGrass == true)
        {
            grassMesh = this.transform.GetChild(0).gameObject;
            grassMesh.SetActive(true);

            float randomRotation = Random.Range(0, 360);
            grassMesh.transform.rotation = Quaternion.Euler(0, randomRotation, 0);
            _grassLevel = 1f;
        }
    }

    //Reduces grass when being run
    public void MowGrass(float mowSpeed)
    {
        _grassLevel -= mowSpeed;
        this.transform.localScale -= new Vector3(0, mowSpeed, 0);

        //If grass falls below this then it's made inactive and the grass is no longer overgrown
        if(_grassLevel <= 0.15)
        {
            this.transform.localScale = new Vector3(transform.localScale.x, 1, transform.localScale.z);
            grassMesh.SetActive(false);
            _overgrownGrass = false;
        }
    }
}
