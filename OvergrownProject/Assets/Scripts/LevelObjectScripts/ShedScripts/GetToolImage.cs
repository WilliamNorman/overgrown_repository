﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetToolImage : MonoBehaviour {

    public int slotNumber;
        
  
    // Update is called once per frame
    void Update () {
        
        GetComponent<Image>().sprite = Tool.publicListOfTools[Shed.currentTool + slotNumber].ToolImage;

    }
}
