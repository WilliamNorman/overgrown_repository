﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shed : MonoBehaviour
{

    public GameObject ShedPanel, firstSpace, thirdSpace,maxToolsPanel;
    bool isPanelUp;
    public static int currentTool;
    [SerializeField]
    int maxNumberOfTools = 3; 
    public static int currentNumberOfTools;

    #region Door Movement
    private GameObject Door1;
    private GameObject Door2;
    private bool OpenDoors;
    #endregion

    void Start()
    {
        Door1 = GameObject.Find("Door 1");
        Door2 = GameObject.Find("Door 2");
        OpenDoors = false;
        ShedPanel.transform.position = Camera.main.WorldToScreenPoint(this.transform.position);
    }

    private void OnTriggerStay(Collider other)
    {
        FixedCameraMove fixedCameraMove = other.gameObject.GetComponent<FixedCameraMove>();//Get componet from the player

        if (other.gameObject.tag == "Player")//Checks if it is the player
        {
            ObjectPickUp objectPickUp = other.gameObject.GetComponent<ObjectPickUp>(); // get component from player
            OpenDoors = true;
            if (objectPickUp._holdingObject == true)// checks to see if the player is holding an object
            {
                if (Input.GetButtonDown("UseTool"))
                {
                    //Destroys the object if useTool is pressed
                    if(objectPickUp._currentObject.tag == "Tool")
                    {
                        Object.Destroy(objectPickUp._currentObject); 
                        objectPickUp._holdingObject = false;
                        currentNumberOfTools--;
                    }
                    
                }

            }

            else// if the player is not holding an object
            {
                
                if (Input.GetButtonDown("UseTool"))
                {
                    //Open up shed menu
                    if (isPanelUp == false)
                    {
                        ShedPanel.SetActive(true);
                        isPanelUp = true;
                        fixedCameraMove.MovementScale = 0; //pauses the player's movement
                        fixedCameraMove.RotationScale = 0;
                    }

                    else if(isPanelUp == true) 
                    {
                        //If shed menu is already open close it
                        ShedPanel.SetActive(false);
                        isPanelUp = false;
                        fixedCameraMove.MovementScale = 1;
                        fixedCameraMove.RotationScale = 1;
                    }
                }

                //Go to the next item
                if (Input.GetKeyDown(KeyCode.D) && currentTool < Tool.publicListOfTools.Count - 2)
                {
                     currentTool++;
                }

                //Go to the previous item
                if (Input.GetKeyDown(KeyCode.A) && currentTool > -1)
                {
                     currentTool--;
                }

                // The third slot disappears
                 if (currentTool >= Tool.publicListOfTools.Count - 2)
                 {
                     thirdSpace.SetActive(false);
                 }
                 else
                 {
                     thirdSpace.SetActive(true);
                 }


                 //the first slot disappears
                 if (currentTool <= -1)
                 {
                     firstSpace.SetActive(false);
                 }
                 else
                 {
                     firstSpace.SetActive(true);
                 }

                 //instantiate the selected item
                 if (currentNumberOfTools < maxNumberOfTools)//Checks to see if the max number of tools have been created
                 {
                     if (Input.GetButtonDown("PickUp") && isPanelUp == true)
                     {
                        GameObject newTool = Instantiate(Tool.publicListOfTools[currentTool + 1].ToolPrefab, new Vector3(gameObject.transform.position.x + 1, gameObject.transform.position.y, transform.position.z -1), Quaternion.identity);
                        objectPickUp.CurrentObject = newTool;

                        ShedPanel.SetActive(false);
                        isPanelUp = false;
                        fixedCameraMove.MovementScale = 1;
                        fixedCameraMove.RotationScale = 1;
                        currentNumberOfTools++;
                    }

                 }

                 else//Lets the player know they can't create anymore
                 {
                    if (Input.GetButtonDown("PickUp") && isPanelUp == true)
                    {
                        StopAllCoroutines();
                        StartCoroutine(ShowTemporaryMessage());
                    }                      
                 }
                 

            }

        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            FindObjectOfType<_AudioManager>().PlayAudioClip(GetComponent<AudioSource>(), "ShedClose");
            OpenDoors = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            FindObjectOfType<_AudioManager>().PlayAudioClip(GetComponent<AudioSource>(), "ShedOpen");
            OpenDoors = true;
        }
    }


    IEnumerator ShowTemporaryMessage()
    {
        maxToolsPanel.SetActive(true);
        yield return new WaitForSeconds(3);
        maxToolsPanel.SetActive(false);
    }

    private void Update()
    {
        if (OpenDoors == true)
        {

            Door1.transform.rotation = Quaternion.Euler(0, -200, 0);
            Door2.transform.rotation = Quaternion.Euler(0, 200, 0);
        }
        if (OpenDoors == false)
        {
            Door1.transform.rotation = Quaternion.Euler(0, -90, 0);
            Door2.transform.rotation = Quaternion.Euler(0, 90, 0);
        }
    }
}
    

    
         


