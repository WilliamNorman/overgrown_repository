﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tool : MonoBehaviour {

    string toolName;
    Sprite toolImage;
    GameObject toolPrefab;
    public List<Tool> listOfTools;
    public static List<Tool> publicListOfTools;
   

    public Tool(string _toolName, Sprite _toolImage, GameObject _toolPrefab)
    {
        toolName = _toolName;
        toolImage = _toolImage;
        toolPrefab = _toolPrefab;
    }

    public Sprite ToolImage
    {
        get
        {
            return toolImage;
        }

         set
        {
            toolImage = value;
        }
    }


    public string ToolName
    {
        get
        {
            return toolName;
        }

        set
        {
            toolName = value;
        }
    }

    public GameObject ToolPrefab
    {
        get
        {
            return toolPrefab;
        }

        set
        {
            toolPrefab = value;
        }
    }



    // Use this for initialization
    void Start () {

        //Add tools to list
        listOfTools.Add(new Tool("Shovel", Resources.Load<Sprite>("Spade"), Resources.Load<GameObject>("Spade")));
        listOfTools.Add(new Tool("Seeds", Resources.Load<Sprite>("Seeds"), Resources.Load<GameObject>("SeedPacket")));
        listOfTools.Add(new Tool("Axe", Resources.Load<Sprite>("Axe"), Resources.Load<GameObject>("Axe")));
        listOfTools.Add(new Tool("Weed Killer", Resources.Load<Sprite>("WeedKiller"), Resources.Load<GameObject>("WeedKiller")));
        listOfTools.Add(new Tool("Shears", Resources.Load<Sprite>("Shears"), Resources.Load<GameObject>("Shears")));
        listOfTools.Add(new Tool("Watering Can", Resources.Load<Sprite>("WateringCan"), Resources.Load<GameObject>("WateringCan")));

        //Add to the public list
        publicListOfTools = listOfTools;

    }
}
