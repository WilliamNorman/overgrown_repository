﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

//Written by William Norman
public class ObjectPickUp : MonoBehaviour {

    [SerializeField]
    LayerMask _targetLayer;// Layer the objects will be on
    [SerializeField]
    float _collisionRadius = 0.5f;
    [SerializeField]
    float capsuleHeight = 1f;
    [SerializeField]
    float throwChargeRate;
    [SerializeField]
    float throwMaxCharge;

    DefaultTool _defaultTool;
    float _throwCharge;
    Transform _objectSocket;
    Transform _toolSocket;// The transforms which the objects will be parented to
    FixedCameraMove _fixedMovement;
    public bool _holdingObject;// Is the character holding an object?
    public GameObject _currentObject;// The current object being held
    public PickUpPhysics _pickUpPhysics;

    public GameObject CurrentObject// The CurrentObject can only be set if nothing is being held, it's automatically oriented once picked up
    {
        set
        {
            // This will nullify the variables and set holdingObject to false
            if (value == null)
            {
                _currentObject = null;
                _pickUpPhysics = null;
                _holdingObject = false;
                return;
            }
            //This will store the new object and orientate it
            if (!_holdingObject)
            {
                _currentObject = value;
                _pickUpPhysics = value.GetComponent<PickUpPhysics>();
                _holdingObject = true;
                OrientateObject();
                return;
            }       
            //If the player is holding something then this'll drop the current one and store/orientate the new one
            if(_holdingObject && _pickUpPhysics._beingPickedUp == false)
            {
                DropObject();
                _currentObject = value;
                _pickUpPhysics = value.GetComponent<PickUpPhysics>();
                _holdingObject = true;
                OrientateObject();
                return;
            }          
        }
        get
        {
            if (_holdingObject)
            {
                return _currentObject;
            }
            else
                return null;
        }
    }

    void Awake () {
        // The transform.Find methods will get the transforms for the different sockets
        _objectSocket = transform.Find("ObjectSocket");
        _toolSocket = transform.Find("RightHand");
        _toolSocket = _toolSocket.Find("ToolSocket");
        _holdingObject = false;
        _fixedMovement = GetComponent<FixedCameraMove>();
        _defaultTool = GetComponent<DefaultTool>();
	}

    #region Update Method
    void Update () {

        // This is for picking up the object when the player's hands are empty
        if (Input.GetButtonDown("PickUp") && !_holdingObject)
        {
            PickUpObject();
        }
        //If the player is holding something then they'll drop the current object
        else if(Input.GetButtonDown("PickUp") && _holdingObject)
        {
            if (_pickUpPhysics._beingPickedUp == false)
            {
                DropObject();
                PickUpObject();
            }
        }

        //Throws object if player's holding one
        if (_holdingObject && _pickUpPhysics._beingPickedUp == false)
        {
            if (Input.GetButtonDown("Throw"))
            {
                ThrowObject();
                FindObjectOfType<_AudioManager>().PlayAudioClip(GetComponents<AudioSource>()[1], "ThrowObject");
                _fixedMovement.FreezeMovement(0.2f);
            }
        }

        //Uses tool
        if (_holdingObject && _currentObject.CompareTag("Tool"))
        {
            if (Input.GetButtonDown("UseTool"))
                _fixedMovement.SlowMovement();
            if (Input.GetButton("UseTool"))// Uses the tool if one is equipped
            {
                _currentObject.SendMessage("UseTool", this.transform);
            }
            else if (Input.GetButtonUp("UseTool"))
                _fixedMovement.FastMovement();
        }

        //Uses default tool (hands) if no tool is available
        if(!_holdingObject && Input.GetButton("UseTool"))
        {
            GameObject tempObject = _defaultTool.UseHands(this.transform);

            if(tempObject != null && tempObject.CompareTag("Item"))
            {
                CurrentObject = tempObject;
            }
        }
    }
    #endregion

    #region Throw, Drop and PickUp object methods
    void ThrowObject()
    {
        _currentObject.transform.parent = null;
        _pickUpPhysics.ResetRotationAndEnablePhysics(this.transform);
        _pickUpPhysics.RB.AddForce(this.transform.forward * 7, ForceMode.Impulse);
        CurrentObject = null;
    }

    public void DropObject()
    {
        if (_holdingObject == true)
        {
            _currentObject.transform.parent = null;
            _pickUpPhysics.RB.AddForce(this.transform.forward * 4f, ForceMode.Impulse);
            _pickUpPhysics.ResetRotationAndEnablePhysics(this.transform);

            CurrentObject = null;
        }
    }

    void PickUpObject()
    {
        //Finds and populates array with relevant objects
        Collider[] _foundObjects;

        float playerRadius = GetComponent<CapsuleCollider>().radius;// Get player's radius
        Vector3 startVect = this.transform.position + (this.transform.forward * playerRadius);
        startVect += this.transform.forward * _collisionRadius;
        _foundObjects = Physics.OverlapSphere(startVect, _collisionRadius, _targetLayer, QueryTriggerInteraction.Collide);

        if (_foundObjects.Length == 0)// if no object is found then return
            return;

        //Orders objects by distance to player
        _foundObjects = _foundObjects.OrderBy(_foundObject => Vector3.Distance(this.transform.position, _foundObject.transform.position)).ToArray();// Assigns the currentObject and sets holdingObject to true

        //Checks if any objects have the correct tag or not
        int index = 0;
        while(index < _foundObjects.Length)
        {
            if(_foundObjects[index].GetComponent<PickUpPhysics>()._beingPickedUp == false)
            {
                if (_foundObjects[index].CompareTag("Tool") || _foundObjects[index].CompareTag("Item"))
                {
                    CurrentObject = _foundObjects[index].gameObject;
                    index = _foundObjects.Length;
                }
            }
            index++;
        }
    }
    #endregion

    #region OrientateObject methods
    //Orientates object using two different pivot points
    void OrientateObject()
    {
        if (_currentObject.CompareTag("Tool"))
        {
            _pickUpPhysics.OrientateAndDisablePhysics(_toolSocket);
        }
        else if (_currentObject.CompareTag("Item"))
        {
            _pickUpPhysics.OrientateAndDisablePhysics(_objectSocket);
        }
    }
    #endregion
}
