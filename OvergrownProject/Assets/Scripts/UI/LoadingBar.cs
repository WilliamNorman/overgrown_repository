﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Written by William Norman
public class LoadingBar : MonoBehaviour {

    Image _loadingBarImage;
    GameObject _warningImage;
    GameObject _mainCanvas;

	void Awake () {        
        _loadingBarImage = this.transform.GetChild(0).GetComponent<Image>();
        _loadingBarImage.fillAmount = 0;
        _warningImage = _loadingBarImage.transform.GetChild(0).gameObject;
        _mainCanvas = GameObject.Find("LoadingBarCanvas");
        this.transform.SetParent(_mainCanvas.transform, false);         
    }
	
	public void SetBarAmount(float currentNum, float maxNum)
    {
        _loadingBarImage.fillAmount = currentNum / maxNum;
    }

    public void MoveLoadingBar(Transform targetTransform)
    {
        _loadingBarImage.transform.position = Camera.main.WorldToScreenPoint(targetTransform.position);
    }

    public void ShowWarning()
    {
        _warningImage.SetActive(true);
    }

    public void HideWarning()
    {
        _warningImage.SetActive(false);
    }


}
