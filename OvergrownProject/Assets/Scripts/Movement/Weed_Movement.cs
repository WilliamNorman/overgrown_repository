﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weed_Movement : MonoBehaviour
{
    public GameObject Gameobject;
    private GameObject player;
    private Component playerScript;

    public Animator _animator;

    ParticleSystem megaWeedParticleSystem;

    public int TEST;

    #region point Selection Variables
    //A list of game objects with the tag "Pathable" 
    private List<GameObject> pointList = new List<GameObject>();
    //The current point selected from the list 
    private GameObject point;
    private bool routineRunning;
    #endregion

    #region movement Variables

    //The bool "nearPlayer" will be toggled on and off in the Update function depending on
    //if the "distanceToPlayer" is greater than or less than 3 
    public float speed = 5f;
    public bool nearPlayer = false;
    public float rotationSpeed = 5f;
    private float distanceToPlayer;
    private Quaternion _lookRotation;
    private Vector3 direction;


    #endregion


    #region added Randomness
    //These three floats create the Vector3 "addedRandomness" 
    //Each time a new point is chosen to move towards the "addedRandomness" is added to the transform of the chosen point
    //This means that the weed isnt walking to exactly the same set of transforms over and over again
    public Vector3 addedRandomness;
    float x;
    float y = 0.3f;
    float z;
    #endregion

    #region Pushing the player

    public bool playerPushed = false;
    public List<GameObject> runAwayList = new List<GameObject>();
    public GameObject runPoint;
    private bool isSpinning = false;
    private float turnTime = 1f;

    #endregion


    void Awake()
    {
        
        _animator = GetComponentInChildren<Animator>();
        player = GameObject.FindGameObjectWithTag("Player");// Script will now find the player automatically
        playerScript = player.GetComponent<ObjectPickUp>();
    }

    private void Start()
    {
        megaWeedParticleSystem = GetComponentInChildren<ParticleSystem>();

        //Find every gameobject in the scene with the tag "Pathable" and add them to the list
        foreach (GameObject fooObj in GameObject.FindGameObjectsWithTag("Pathable"))
        {
            pointList.Add(fooObj);
        }
        //Find the runAway points on the map and add them to the runAway list
        foreach (GameObject fooObj in GameObject.FindGameObjectsWithTag("RunAway"))
        {
            runAwayList.Add(fooObj);
        }

        StartCoroutine("Movement");
        TEST++;
        routineRunning = true;
        
    }


    #region IEnumerators

    private IEnumerator Movement()
    {
        //Coroutine function
        //1: Create new values for the Vector3 "addedRandomness" 
        //2: Choose a random gameobject from the list
        //3: Add the Vector3 and gameobject transform together

        if(playerPushed == true)
        {
            
            //Play a runAway animation
            x = UnityEngine.Random.Range(-0.3f, 0.3f);
            z = UnityEngine.Random.Range(-0.3f, 0.3f);

            addedRandomness = new Vector3(x, y, z);

            var rand = new System.Random();

            runPoint = runAwayList[rand.Next(0, runAwayList.Count)];

            addedRandomness = addedRandomness + runPoint.transform.position;
            yield return new WaitForSeconds(5);
            
            yield break;

        }

        else
        {
            while (nearPlayer == false)
            {
                _animator.SetBool("IsMoving", true);
                x = UnityEngine.Random.Range(-0.3f, 0.3f);
                z = UnityEngine.Random.Range(-0.3f, 0.3f);

                addedRandomness = new Vector3(x, y, z);

                var rand = new System.Random();

                point = pointList[rand.Next(0, pointList.Count)];

                addedRandomness = addedRandomness + point.transform.position;
                yield return new WaitForSeconds(3);
            }
        }
        
    }

    private IEnumerator Push()
    {

        spin();
        playerPushed = true;
        StopCoroutine("Movement");
        yield return StartCoroutine("Movement");
        
        playerPushed = false;
        yield break;
        
    }

    private IEnumerator SpinPlayer()
    {
        _animator.SetBool("Bite", true);
        isSpinning = true;
        player.GetComponent<ObjectPickUp>().DropObject();
        player.GetComponent<FixedCameraMove>().FreezeMovement(1.2f);
       
        
        var time = 0f;

        while (time < turnTime)
        {
            time += Time.deltaTime;
            
            player.transform.Rotate(new Vector3(0, 15, 0), Space.Self);
            
            yield return null;
        }
        _animator.SetBool("Bite", false);
        

        player.transform.localRotation = player.transform.localRotation;
        isSpinning = false;
        yield break;

    }

    #endregion

    private void spin()
    {
        if(isSpinning == false)
        {

            StartCoroutine("SpinPlayer");
        }
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (playerPushed == false)
            {
                StartCoroutine("Push");
                playerPushed = true;
            }
            
        }
    }

    private void Update()
    {

        float move = speed * Time.deltaTime;

        
        #region Player detection

        distanceToPlayer = Vector3.Distance(Gameobject.transform.position, player.transform.position);

        if (playerPushed == false)
        {
            if (distanceToPlayer >= 4)
            {
                nearPlayer = false;

                if (routineRunning == false)
                {
                    StartCoroutine("Movement");
                    TEST++;
                    routineRunning = true;
                }
            }
            else if (distanceToPlayer < 4)
            {
                //Play a chasing animation

                nearPlayer = true;

                if (routineRunning == true)
                {
                    StopCoroutine("Movement");
                    TEST--;
                    routineRunning = false;
                }
            }
        }
        
        #endregion

        #region Movement

        _lookRotation = Quaternion.LookRotation(direction);

        if (playerPushed == true)
        {
            direction = (Gameobject.transform.position - addedRandomness);
            transform.rotation = Quaternion.Slerp(transform.rotation, _lookRotation, Time.deltaTime * rotationSpeed);
            transform.position = Vector3.MoveTowards(Gameobject.transform.position, addedRandomness, move);
        }

        else if (playerPushed == false)
        {
            if (nearPlayer == true)
            {
                direction = (Gameobject.transform.position - player.transform.position);


                transform.rotation = Quaternion.Slerp(transform.rotation, _lookRotation, Time.deltaTime * rotationSpeed);
                transform.position = Vector3.MoveTowards(Gameobject.transform.position, player.transform.position, move);
            }

            else if (nearPlayer == false)
            {
                direction = (Gameobject.transform.position - addedRandomness);
                transform.rotation = Quaternion.Slerp(transform.rotation, _lookRotation, Time.deltaTime * rotationSpeed);
                transform.position = Vector3.MoveTowards(Gameobject.transform.position, addedRandomness, move);
            }



            if (Vector3.Distance(Gameobject.transform.position, point.transform.position) < 0.2f)
            {
                if (routineRunning == true)
                {
                    StopCoroutine("Movement");
                    TEST--;
                    StartCoroutine("Movement");
                    TEST++;
                    routineRunning = true;
                }

            }
        } 
        #endregion
    }

}
