﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 8 Directional movement for the character, it also stops and faces the current direction when no input is detected
/// </summary>
public class FixedCameraMove : MonoBehaviour
{
    public float MovementScale = 1;
    public float RotationScale = 1;
    public bool isMoving;
    [HideInInspector]
    public Rigidbody _rb;

    [SerializeField]
    float Velocity = 5f;// controls their movement speed
    [SerializeField]
    float TurnSpeed = 10;// controls their turning speed

    protected bool anyPlayerInput;// is there any input from the player
    
    Vector2 _input;// stores input in the x/y coordinates 
    float _angle;  
    ParticleSystem _particleSystem;
    Quaternion _targetRotation;
    


    protected virtual void Awake()
    {
        _rb = GetComponent<Rigidbody>();
        _particleSystem = GetComponentInChildren<ParticleSystem>();
    }

    protected virtual void Update()
    {
        GetInput();

        //If there's no input then return
        if (Mathf.Abs(_input.x) < 1 && Mathf.Abs(_input.y) < 1 && this.gameObject.name != "Lawnmower")// .Abs means absolute, this will make the number positive
        {
            _particleSystem.Stop();// Stop particles if there's no input           
            anyPlayerInput = false; return;
        }
        else
        {
            if (_particleSystem.isPlaying == false)// If there's input and it's not playing then set particles to play
            {
                _particleSystem.Play();                
            }

            anyPlayerInput = true;
        }
    }

    //Handles all the movement and rotation
    void FixedUpdate()
    {
        if (anyPlayerInput)
        {
            CalculateDirection();
            Rotate();

            MovePosition();
        }
    }

    //Moves player's position, can be accessed externally
    public void MovePosition()
    {
        float tempVelocity = Velocity * MovementScale * Time.fixedDeltaTime * Time.timeScale;
        _rb.MovePosition(this.transform.position + this.transform.forward * tempVelocity);   

    }

    //Store input values
    void GetInput()
    {
        _input.x = Input.GetAxisRaw("Horizontal");
        _input.y = Input.GetAxisRaw("Vertical");
    }

    //Calculate the direction to face towards
    void CalculateDirection()
    {
        //this calculates the angle from the input and convert it to degrees
        _angle = Mathf.Atan2(_input.x, _input.y);   
        _angle = Mathf.Rad2Deg * _angle;            
    }

    //Rotate character towards calculated angle
    void Rotate()
    {
        _targetRotation = Quaternion.Euler(0, _angle, 0);// converts Vector3 to Eular
        float tempTurnSpeed = TurnSpeed * RotationScale * Time.deltaTime * Time.timeScale;
        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, _targetRotation, tempTurnSpeed);//Slerp is basically lerp. This'll interpolate between the different rotations
    }


    public void SlowMovement()
    {
        MovementScale = 0.15f;
    }
    
    //Renable fast movement after a set amount of time
    public void SlowMovement(float timeToWait)
    {
        MovementScale = 0.15f;
        Invoke("FastMovement", timeToWait);
    }
   
    //Freezes movement for set amount of time
    public void FreezeMovement(float timeToWait)
    {
        RotationScale = 0f;

        MovementScale = 0f;
        Invoke("FastMovement", timeToWait);
    }

    //Resets movement to normal
    public void FastMovement()
    {
        RotationScale = 1f;
        MovementScale = 1;
    }

    void OnDisable()
    {
        _particleSystem.Stop();
    }
}
