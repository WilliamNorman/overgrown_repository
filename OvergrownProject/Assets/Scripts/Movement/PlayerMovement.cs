﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : FixedCameraMove {

    public Animator _animator;
    public AudioSource _audioSource;

    protected override void Awake()
    {
        base.Awake();
        _animator = GetComponentInChildren<Animator>();
        _audioSource = GetComponents<AudioSource>()[0];
    }

    //performs base update and also manages animation/sound
    protected override void Update()
    {
        base.Update();

        if(anyPlayerInput && Time.timeScale > 0f)
        {
            _animator.SetBool("IsMoving", true);
            FindObjectOfType<_AudioManager>().PlayAudioClip(_audioSource, "Footsteps", true);
        }
        else
        {
            _animator.SetBool("IsMoving", false);
            FindObjectOfType<_AudioManager>().StopPlayingAudio(_audioSource);
        }
    }
}
