﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Written by William Norman
public class PickUpPhysics : MonoBehaviour {

    [SerializeField]
    float _pickUpRotationSpeed = 1f;
    [SerializeField]
    float _throwRotationSpeed = 1f;

    public bool _keepObjectUpright;
    public bool _disablePhysicsAtStart;
    //[HideInInspector]
    public bool _beingPickedUp = true;
    public Rigidbody RB;

    bool _beingHeld = false;
    bool _isTool = false;

    Collider _physicsCollider;   
    public float _timeNum;
    Vector3 _playerRotation;
    Transform _pivotPoint;
    GameObject _toolHolderMesh;


	void Awake() {
        // Get rigidbody, PivotPoint and toolHolderMesh
        RB = GetComponent<Rigidbody>();       
        _pivotPoint = transform.GetChild(0);       
        
        // Gets all the colliders and stores the non-trigger collider 
        Collider[] tempArray = GetComponentsInChildren<Collider>();
        if (tempArray.Length < 1)
            tempArray = GetComponents<Collider>();
        
        for (int i = 0; i < tempArray.Length; i++)
        {
            if (tempArray[i].isTrigger == false)
                _physicsCollider = tempArray[i];
        }

        // Sets isTool by checking the gameObject's tag
        if (this.gameObject.tag == "Tool")
        {
            _isTool = true;
            _toolHolderMesh = transform.GetChild(1).gameObject;
        }
        else
            _isTool = false;
                          
        // Disables physics on awake
        if(_disablePhysicsAtStart)
        {
            RB.useGravity = false;
            RB.isKinematic = true;
            _physicsCollider.isTrigger = true;
        }
    }

    // Locks the x/y rotation values to 0, this is only applied to tools
    void LateUpdate()
    {
        if(RB.isKinematic == false)
            transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y, 0);
    }

    
    public void DisablePhysics()
    {
        RB.isKinematic = true;
        RB.useGravity = false;
        _physicsCollider.isTrigger = true;
    }

    public void EnablePhysics()
    {
        RB.isKinematic = false;
        RB.useGravity = true;
        _physicsCollider.isTrigger = false;
    }

    // This disables all physics on the object and starts a coroutine that lerps this object to the player
    public void OrientateAndDisablePhysics(Transform socketTransform)
    {
        StopAllCoroutines();
        DisablePhysics();

        // Makes the pivot point this object's parent
        _pivotPoint.parent = null;
        this.transform.parent = _pivotPoint;

        if (_isTool)
            _toolHolderMesh.SetActive(false);

        _beingHeld = true;
        _beingPickedUp = true;
        StartCoroutine(LerpToolToPlayer(socketTransform));
    }

    // This enables physics and lerps to the player's rotation
    public void ResetRotationAndEnablePhysics(Transform playerTransform)
    {
        StopAllCoroutines();
        EnablePhysics();

        if (_isTool)
            _toolHolderMesh.SetActive(true);

        _beingHeld = false;
        _beingPickedUp = true;

        CheckForObstructions(playerTransform.gameObject);
        StartCoroutine(ResetRotation(playerTransform));
    }

    //Checks for obstructions to stop objects from escaping the level/getting stuck
    void CheckForObstructions(GameObject _player)
    {
        CapsuleCollider _capsule = GetComponent<CapsuleCollider>();
        float _radius = _capsule.radius * this.transform.localScale.x;
        float _height = _capsule.height * this.transform.localScale.x;
        _height -= _radius * 2f;

        Vector3 _capsuleCenter = this.transform.position + new Vector3(0, _capsule.center.y, _capsule.center.z) * this.transform.localScale.x;
        Vector3 _capsuleTop = _capsuleCenter + new Vector3(0, _height, 0);
        Vector3 _capsuleBottom = _capsuleCenter - new Vector3(0, _height, 0);

        Collider[] collidingObjects = Physics.OverlapCapsule(_capsuleBottom * 0.1f, _capsuleTop * 0.1f, _capsule.radius * 0.7f, ~0, QueryTriggerInteraction.Ignore);
        bool isObstructed = false;
        
        foreach (Collider _collider in collidingObjects)
        {
            if(_collider.gameObject.tag != "Player" && _collider.gameObject != this.gameObject)
            {
                Debug.Log(_collider.gameObject.name);
                isObstructed = true;
            }
        }

        if(isObstructed)
        {
            CapsuleCollider collider = _player.GetComponent<CapsuleCollider>();
            float playerHeight = ((collider.height / 2) + collider.radius + collider.center.y) * _player.transform.localScale.x;
            this.transform.position = _player.transform.position + new Vector3(0, playerHeight, 0);
            RB.AddForce(_player.transform.forward * 2.5f, ForceMode.Impulse);
        }
    }

    IEnumerator LerpToolToPlayer(Transform socketTransform)
    {
        float i = 0;// Tracks how much to rotate the object

        // This loop will lerp the rotation and position with the target being the socket transform
        // Once it's close enough to the target transform, it'll end the loop
        while (Vector3.Distance(_pivotPoint.position, socketTransform.position) > 0.1)
        {
            i += Time.deltaTime * _pickUpRotationSpeed * Time.timeScale;

            //newRotation = Quaternion.Euler(socketTransform.rotation.eulerAngles.x + X_RotationOfset, socketTransform.rotation.eulerAngles.y, socketTransform.rotation.eulerAngles.z);
                
            _pivotPoint.rotation = Quaternion.Slerp(_pivotPoint.rotation, socketTransform.rotation, i);
            _pivotPoint.position = Vector3.Lerp(_pivotPoint.position, socketTransform.position, i);
            yield return null;
        }

        // sets rotation and position to ensure that they match the target transform
        _pivotPoint.rotation = socketTransform.rotation;
        _pivotPoint.position = socketTransform.transform.position;

        // This nullify's the pivotPoint parent, sets this objects parent to the player's socket and makes the pivot point a child once more
        _pivotPoint.parent = null;        
        this.transform.parent = socketTransform;
        _pivotPoint.parent = this.transform;

        _beingPickedUp = false;
    }

    //Rotates object to allign with player's rotation
    IEnumerator ResetRotation(Transform playerTransform)
    {
        float i = 0;
        float playerStartY = playerTransform.rotation.eulerAngles.y;

        while (i <= 1)
        {
            i += Time.deltaTime * _throwRotationSpeed;
            Quaternion newRotation = Quaternion.Euler(0, playerStartY, 0);

            //this.transform.rotation = Quaternion.Slerp(this.transform.rotation, newRotation, i);
            RB.MoveRotation(Quaternion.Slerp(this.transform.rotation, newRotation, i));
            yield return null;         
        }
        Quaternion finalRotation = Quaternion.Euler(0, playerStartY, 0);
        this.transform.rotation = finalRotation;

        _beingPickedUp = false;        
        yield return null;  
    }

}
