﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnTextOff : MonoBehaviour {

    public static float turnOffTimer = 3;

	// Update is called once per frame
	void Update () {

        turnOffTimer -= Time.deltaTime;
        if(turnOffTimer < 0)
        {
            gameObject.SetActive(false);
        }

		
	}
}
