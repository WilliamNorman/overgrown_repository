﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public  class CharacterCreator : MonoBehaviour {

    public List<GameObject> partListHead = new List<GameObject>();
    public GameObject currentPartHead;
    public int currentPartHeadInt;

    public List<GameObject> partListBody = new List<GameObject>();
    public GameObject currentPartBody;
    public int currentPartBodyInt;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        #region Head 
        if (PlayerPrefs.HasKey("currentPartHead"))
        {
            currentPartHeadInt = PlayerPrefs.GetInt("currentPartHead");
        }

        foreach (Transform child in transform)
        {
            if (child.tag == "Part")
            {
                
                partListHead.Add(child.gameObject);
                currentPartHead = partListHead[0];

            }
        }
        
        #endregion
        if (PlayerPrefs.HasKey("currentPartBody"))
        {
            
            currentPartBodyInt = PlayerPrefs.GetInt("currentPartBody");
        }
        foreach (Transform child in transform)
        {
            if (child.tag == "PartBody")
            {
                
                partListBody.Add(child.gameObject);
                currentPartBody = partListBody[0];

            }
        }
        currentPartHead = partListHead[currentPartHeadInt];
        currentPartHead.SetActive(true);

        currentPartBody = partListBody[currentPartBodyInt];
        currentPartBody.SetActive(true);
    }

    public void ChangeObjectHead()
    {
        currentPartHead.SetActive(false);
        currentPartHeadInt++;
        if (currentPartHeadInt <= (partListHead.Count - 1))
        {
            
            currentPartHead.SetActive(false);
            currentPartHead = partListHead[currentPartHeadInt];
            currentPartHead.SetActive(true);
        }
        else if(currentPartHeadInt > (partListHead.Count -1))
        {
            
            currentPartHeadInt = 0;
            currentPartHead.SetActive(false);
            currentPartHead = partListHead[currentPartHeadInt];
            currentPartHead.SetActive(true);
        }
        
    }

    public void ChangeObjectBody()
    {
        currentPartBody.SetActive(false);
        currentPartBodyInt++;
        if (currentPartBodyInt <= (partListBody.Count - 1))
        {

            currentPartBody.SetActive(false);
            currentPartBody = partListBody[currentPartBodyInt];
            currentPartBody.SetActive(true);
            PlayerPrefs.SetInt("currentPartHead", PlayerPrefs.GetInt("currentPartHead", 0) + currentPartHeadInt);
        }
        else if (currentPartBodyInt > (partListBody.Count - 1))
        {

            currentPartBodyInt = 0;
            currentPartBody.SetActive(false);
            currentPartBody = partListBody[currentPartBodyInt];
            currentPartBody.SetActive(true);
            PlayerPrefs.SetInt("currentPartHead", PlayerPrefs.GetInt("currentPartHead", 0) + currentPartHeadInt);
        }

    }

    private void OnDisable()
    {
        PlayerPrefs.SetInt("currentPartHead", currentPartHeadInt);
        PlayerPrefs.SetInt("currentPartHead", PlayerPrefs.GetInt("currentPartHead", 0) + currentPartHeadInt);

        PlayerPrefs.SetInt("currentPartBody", currentPartBodyInt);
        PlayerPrefs.SetInt("currentPartBody", PlayerPrefs.GetInt("currentPartBody", 0) + currentPartBodyInt);
        PlayerPrefs.Save();
    }

    public void Save()
    {
        PlayerPrefs.Save();
    }

}
