﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Written by William Norman
public class DefaultTool : BaseTool
{

    public GameObject UseHands(Transform playerTransform)//This to picks up small weeds from the soil and returns the gameobject to ObjectPickUp
    {
        GameObject _targetObject;
        _targetObject = CheckForTargets(playerTransform);

        if (_targetObject != null)
        {
            SoilLifecycle soilScript = _targetObject.GetComponent<SoilLifecycle>();

            if (soilScript.CurrentState == SoilStates.SmallWeed)
            {
                _targetObject =  soilScript.RemoveSmallWeed();
            }
        }
        return _targetObject;
    }
}
