﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Written by William Norman
public class SeedPacket : BaseTool, ITool
{
    public void UseTool(Transform playerTransform)
    {
        GameObject _targetObject;
        _targetObject = CheckForTargets(playerTransform);

        if (_targetObject != null)
        {
            SoilLifecycle soilScript = _targetObject.GetComponent<SoilLifecycle>();

            //Plants seeds
            if (soilScript.CurrentState == SoilStates.EmptySoil)
            {
                soilScript.PlaceSeeds();
            }
        }
    }
}
