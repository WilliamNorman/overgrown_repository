﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Axe : BaseTool, ITool
{

    [SerializeField]
    float choppingSpeed = 1f;

    public void UseTool(Transform playerTransform)
    {
        GameObject _targetObject;
        _targetObject = CheckForTargets(playerTransform);

        //Chops down tree
        if (_targetObject != null)
        {
            TreeLifeCycle treeScript = _targetObject.GetComponent<TreeLifeCycle>();
            FindObjectOfType<_AudioManager>().PlayAudioClip(GetComponent<AudioSource>(), "Axe");
            treeScript.ChopDownTree();            
        }
    }
}
