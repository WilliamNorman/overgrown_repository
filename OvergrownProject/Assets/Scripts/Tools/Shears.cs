﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Written by William Norman
public class Shears : BaseTool, ITool
{
    public void UseTool(Transform playerTransform)
    {
        GameObject _targetObject;
        _targetObject = CheckForTargets(playerTransform);

        if (_targetObject != null)
        {
            SoilLifecycle soilScript = _targetObject.GetComponent<SoilLifecycle>();

            //Removes large weed
            if (soilScript.CurrentState == SoilStates.LargeWeed)
            {
                FindObjectOfType<_AudioManager>().PlayAudioClip(GetComponent<AudioSource>(), "Shears");
                soilScript.RemoveLargeWeed();
            }
        }
    }
}
