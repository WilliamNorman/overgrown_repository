﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Written by William Norman
public class WeedKiller : BaseTool, ITool
{

    public GameObject smallWeed;

    public void UseTool(Transform playerTransform)
    {
        GameObject _targetObject;
        _targetObject = CheckForTargets(playerTransform);

        
        if (_targetObject != null)
        {

            // Destory large weed
            Destroy(_targetObject);
            // Spawn small weed and get physics script
            PickUpPhysics pickUpPhysics = Instantiate(smallWeed, _targetObject.transform.position, Quaternion.identity).GetComponent<PickUpPhysics>();
            pickUpPhysics.EnablePhysics();
            pickUpPhysics.gameObject.tag = "Item";

            GetComponentInChildren<ParticleSystem>().Play();
            FindObjectOfType<_AudioManager>().PlayAudioClip(GetComponent<AudioSource>(), "WeedKiller");
        }
    }
}
