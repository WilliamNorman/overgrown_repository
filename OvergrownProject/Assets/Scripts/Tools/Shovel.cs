﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Written by William Norman
public class Shovel : BaseTool, ITool
{
    public void UseTool(Transform playerTransform)
    {
        GameObject _targetObject;
        _targetObject = CheckForTargets(playerTransform);

        if (_targetObject != null)
        {
            Stump stumpScript = _targetObject.GetComponent<Stump>();
            FindObjectOfType<_AudioManager>().PlayAudioClip(GetComponent<AudioSource>(), "Spade");
            stumpScript.DigUpStump();
      
        }
    }

}
