﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Written by William Norman
public class WateringCan : BaseTool, ITool {

    [SerializeField]
    float _wateringSpeed = 1f;
    [SerializeField]
    float _waterLossScale = 0.5f;
    [SerializeField]
    Material _fullCanMat;
    Material _emptyCanMat;


    [SerializeField]
    GameObject _waterBarPrefab;
    Image _waterBar;
    Camera _mainCamera;
    ParticleSystem _particleSystem;

    float _wateringCanLevel = 0f;
    MeshRenderer _meshRenderer;

    protected override void Awake()
    {
        _meshRenderer = GetComponent<MeshRenderer>();
        _emptyCanMat = _meshRenderer.material;
        _particleSystem = GetComponentInChildren<ParticleSystem>();

        //Sets up water bar UI
        _waterBarPrefab =  Instantiate(_waterBarPrefab, this.transform.position, Quaternion.identity);
        _waterBar = _waterBarPrefab.transform.GetChild(0).GetChild(0).GetComponent<Image>();
        _waterBar.fillAmount = 0f;

        _mainCamera = Camera.main;
        GameObject _mainCanvas = GameObject.Find("LoadingBarCanvas");
        _waterBarPrefab.transform.SetParent(_mainCanvas.transform, false);
        _waterBar.transform.parent.position = _mainCamera.WorldToScreenPoint(this.transform.position);
        base.Awake();
    }

    //Moves and updates water bar UI
    void Update()
    {
        _waterBar.transform.parent.position = _mainCamera.WorldToScreenPoint(this.transform.position);
        _waterBar.fillAmount = _wateringCanLevel;
    }

    public void UseTool(Transform playerTransform)
    {
        GameObject _targetObject;
        _targetObject = CheckForTargets(playerTransform);

        if (_targetObject != null)
        {
            SoilLifecycle soilScript = _targetObject.GetComponent<SoilLifecycle>();

            //If there is a valid target then send water to soil, enable audio and particle effects, if there is no water then change material to empty watering can
            if (soilScript.CurrentState == SoilStates.SeededSoil || soilScript.CurrentState == SoilStates.WateredPlant)
            {
                if (_wateringCanLevel > 0 && soilScript._fullyWatered == false)
                {
                    soilScript.WaterSoil(_wateringSpeed);
                    _wateringCanLevel -= Time.deltaTime * _waterLossScale;

                    StopAllCoroutines();
                    StartCoroutine(AudioLoopRoutine());
                    StartCoroutine(ParticleLoopRoutine(_particleSystem));

                    FindObjectOfType<_AudioManager>().PlayAudioClip(_audioSource, "WateringCan", true);   

                    if(_particleSystem.isPlaying == false)
                        GetComponentInChildren<ParticleSystem>().Play();
                }
                else if(_wateringCanLevel <= 0)
                {
                    _meshRenderer.material = _emptyCanMat;
                }
            }
        }
    }

    public void  FillWateringCan()
    {
        _meshRenderer.material = _fullCanMat;
        _wateringCanLevel = 1f;
    }

}
