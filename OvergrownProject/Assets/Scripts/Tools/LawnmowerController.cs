﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Written by William Norman
public class LawnmowerController : MonoBehaviour {

    FixedCameraMove _movementScript;
    bool _isOccupied = false;
    GameObject _player;
    Transform _playerSocket;


    List<GrassTile> _grassList = new List<GrassTile>();

    [SerializeField]
    float _mowingSpeed = 1f;

    void Awake()
    {
        _movementScript = GetComponent<FixedCameraMove>();
        _movementScript.enabled = false;
        _playerSocket = this.transform.GetChild(0);           
    }

    void Update()
    {
        //How to exit the lawnmower
        if (Input.GetButtonDown("Throw") && _isOccupied)
        { 
            ExitLawnmower();
        }
        
        //Mows grass if any is available
        if (_isOccupied)
        {
            Collider[] grassTiles = Physics.OverlapSphere(this.transform.position, 1f);
            _grassList.Clear();

            if (grassTiles.Length > 0)
            {
                foreach (Collider tile in grassTiles)
                {
                    if (tile.CompareTag("Grass"))
                    {
                        _grassList.Add(tile.GetComponent<GrassTile>());
                    }
                }
            }

            if (_grassList.Count > 0)
            {
                foreach (GrassTile tile in _grassList)
                {
                    tile.MowGrass(Time.deltaTime * _mowingSpeed * Time.timeScale);
                }
            }
        }
    }

    void FixedUpdate()
    {
        //Whilst the script is enabled, the lawnmower will constantly move forward
        //It can still be rotated using FixedCameraMove but movement is done here
        if (_movementScript.enabled == true)
        {
            _movementScript.MovePosition();
        }
    }

    public void EnterLawnmower(GameObject player)
    {
        //Store and disable player
        _player = player;
        _player.GetComponent<PlayerMovement>().enabled = false;
        _player.GetComponent<ObjectPickUp>().enabled = false;
        _player.GetComponent<Collider>().isTrigger = true;
        _player.GetComponent<Rigidbody>().isKinematic = true;

        //Move and parent player
        _player.transform.position = new Vector3(_playerSocket.position.x, _player.transform.position.y, _playerSocket.position.z);
        _player.transform.rotation = _playerSocket.rotation;
        _player.transform.parent = this.transform;

        //Enable lawnmower movement and set isOccupied to true
        _movementScript.enabled = true;
        _isOccupied = true;

        //Play audio and stop player's audio
        FindObjectOfType<_AudioManager>().StopPlayingAudio(_player.GetComponent<PlayerMovement>()._audioSource);
        FindObjectOfType<_AudioManager>().PlayAudioClip(GetComponent<AudioSource>(), "LawnMower"); 

    }

    public void ExitLawnmower()
    {
        //Enable player
        _player.GetComponent<FixedCameraMove>().enabled = true;
        _player.GetComponent<ObjectPickUp>().enabled = true;
        _player.GetComponent<Collider>().isTrigger = false;
        _player.GetComponent<Rigidbody>().isKinematic = false;
        

        //Unparent player and reset it's position
        //_player.transform.position = _playerSocket.position;
        _player.transform.parent = null;
        
        //nullify player variable and disable lawnmower movement
        _player = null;
        _movementScript.enabled = false;
        _isOccupied = false;

        //Stop playing audio
        FindObjectOfType<_AudioManager>().StopPlayingAudio(GetComponent<AudioSource>());

    }

    private void BoolSetDelay(bool isOccupied)
    {
        _isOccupied = !_isOccupied;
    }

    //Handles the player entering the lawnmower
    private void OnTriggerStay(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            ObjectPickUp objectPickUp = other.GetComponent<ObjectPickUp>();
            if(Input.GetButtonDown("UseTool") && objectPickUp._holdingObject == false)
            {
                EnterLawnmower(other.gameObject);                
            }
        }
    }




}
