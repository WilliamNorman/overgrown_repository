﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

//Written by William Norman
public class BaseTool : MonoBehaviour {


    [SerializeField]
    protected LayerMask _targetLayer ;
    [SerializeField]
    protected string _targetTag = "Soil";
    [SerializeField]
    protected float _capsuleHeight = 1;
    [SerializeField]
    protected float _collisionRadius = 1f;
    protected AudioSource _audioSource;
    private Collider[] _foundObjects;


    protected virtual void Awake()
    {
        _audioSource = this.gameObject.GetComponent<AudioSource>();
    }

    
    // This method tries to find the closest target that matches the set parameters. The parameters it recives determines the location/size of the capsule and the target layer/tag
    protected GameObject CheckForTargets(Transform playerTransform)
    {
        _foundObjects = null;

        //Original overlap capsule code
        /*
        // the starting vector is moved to the player's forward vector and then multiplied by the player's radius. This places it right in front of the player
        // After that it's moved forward again to where the center of the capsule will be
        // Lastly it's moved up using capsuleHeight
        float playerRadius = playerTransform.gameObject.GetComponent<CapsuleCollider>().radius;// Get player's radius
        Vector3 startVect = playerTransform.position + (playerTransform.forward * playerRadius);
        startVect += playerTransform.forward * _capsuleRadius;
        startVect += playerTransform.up * _capsuleHeight;

        // The end vector is the same as the start only it's moved to the ground
        Vector3 endVect = new Vector3(startVect.x, 0, startVect.y);

        _foundObjects = Physics.OverlapCapsule(startVect, endVect, _capsuleRadius, _targetLayer);
        */

        //The starting vector finds the front of the player using its radius
        //After that an overlap sphere uses the variables to find the relevant game objects
        float playerRadius = playerTransform.gameObject.GetComponent<CapsuleCollider>().radius;// Get player's radius
        Vector3 startVect = playerTransform.position + (playerTransform.forward * playerRadius);
        startVect += playerTransform.forward * _collisionRadius;
        _foundObjects = Physics.OverlapSphere(startVect, _collisionRadius, _targetLayer, QueryTriggerInteraction.Collide);

        if (_foundObjects.Length == 0)// If no objects are found then it'll return null
            return null;

        _foundObjects = _foundObjects.OrderBy(_foundObjects => Vector3.Distance(playerTransform.position, _foundObjects.transform.position)).ToArray();// This function will order the array using the Vector.Distance, the object closest to the player is placed at index 0 

        for (int i = 0; i < _foundObjects.Length; i++)//Find and return the closest object that has the correct tag
        {
            if(_foundObjects[i].CompareTag(_targetTag))
            {
                return _foundObjects[i].gameObject;
            }
        }

        return null;// Default response if no valid object is found 
    }

    protected IEnumerator AudioLoopRoutine()
    {
        yield return new WaitForSeconds(0.2f);
        FindObjectOfType<_AudioManager>().StopPlayingAudio(_audioSource);
    }

    protected IEnumerator ParticleLoopRoutine(ParticleSystem particleSystem)
    {
        yield return new WaitForSeconds(0.2f);
        particleSystem.Stop();

    }

}
