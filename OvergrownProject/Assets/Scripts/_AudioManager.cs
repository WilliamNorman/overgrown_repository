﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _AudioManager : MonoBehaviour {


    private Dictionary<string, AudioClip> _audioLibrary = new Dictionary<string, AudioClip>();

	void Awake () {
        AudioClip[] _audioClips = Resources.LoadAll<AudioClip>("Audio");
        for(int i = 0; i < _audioClips.Length; i++)
        {
            _audioLibrary.Add(_audioClips[i].name, _audioClips[i]);
        }
	}

    public void AssignAudioClip(AudioSource _audioSource, string _clipName)
    {
        _audioSource.clip = _audioLibrary[_clipName];
    }

    public void StopPlayingAudio(AudioSource _audioSource)
    {
        _audioSource.Stop();
        _audioSource.loop = false;
    }

    /// <summary>
    /// Play an audio clip
    /// </summary>
    public void PlayAudioClip(AudioSource _audioSource, string _clipName)
    {
        AudioClip _newClip =  _audioLibrary[_clipName];
        if(_newClip != null)
        {
            _audioSource.clip = _audioLibrary[_clipName];
            _audioSource.Play();
        }       
        else
            Debug.Log("Cannot find audio clip named: " + _clipName);
    }

    /// <summary>
    /// Play an audio clip with the option to have it loop endlessly
    /// </summary>
    public void PlayAudioClip(AudioSource _audioSource, string _clipName, bool isLooping)
    {
        AudioClip _newClip = _audioLibrary[_clipName];

        // If the clip is already assigned and playing then no change is required
        if(_newClip == _audioSource.clip && _audioSource.isPlaying)
        {
            return;
        }
        else if (_newClip != null)
        {
            _audioSource.clip = _audioLibrary[_clipName];
            _audioSource.Play();
            _audioSource.loop = isLooping;
        }
        else
            Debug.Log("Cannot find audio clip named: " + _clipName);
    }

    /// <summary>
    /// Play an audio clip with a set delay
    /// </summary>
    public void PlayAudioClip(AudioSource _audioSource, string _clipName, float _delayTime)
    {
        AudioClip _newClip = _audioLibrary[_clipName];
        if (_newClip != null)
        {
            _audioSource.clip = _audioLibrary[_clipName];
            _audioSource.PlayDelayed(_delayTime);
        }
        else
            Debug.Log("Cannot find audio clip named: " + _clipName);
    }

    /// <summary>
    /// Play an audio clip and randomize the pitch
    /// </summary>
    public void PlayAudioClip(AudioSource _audioSource, string _clipName, float _minPitch, float _maxPitch)
    {
        float _newPitch = Random.Range(_minPitch, _maxPitch);
        AudioClip _newClip = _audioLibrary[_clipName];
        if (_newClip != null)
        {
            _audioSource.clip = _audioLibrary[_clipName];
            _audioSource.pitch = _newPitch;
            _audioSource.Play();
        }
        else
            Debug.Log("Cannot find audio clip named: " + _clipName);
    }

}
