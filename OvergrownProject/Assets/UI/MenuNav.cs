﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuNav : MonoBehaviour {

    public GameObject tutorialPanel;

    #region MAIN MENU
    public void OnPlay()
    {
        SceneManager.LoadScene("Level Select");
    }

    public void OnQuit()
    {
        Application.Quit();
    }
    #endregion

    #region LEVEL SELECT

    public void OnBack()
    {
        SceneManager.LoadScene("Main Menu");
    }

    public void OnLevel1()
    {
        SceneManager.LoadScene(2);
    }

    public void OnLevel2()
    {
        SceneManager.LoadScene("SecondLevel");
    }

    public void OnLevel3()
    {
        SceneManager.LoadScene("PrototypeScene");
    }

    #endregion

    #region Tutorial
    public void OpenPanel()
    {
        tutorialPanel.SetActive(true);
        Invoke("GoBack", 10f);
    }

    public void GoBack()
    {
        tutorialPanel.SetActive(false);
    }
    #endregion

    #region CHARACTER CREATOR
    public void OpenCreator()
    {
        SceneManager.LoadScene("CharacterCreator");
    }
    #endregion
}
