﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score : MonoBehaviour {

    public static int _oneStar, _twoStar, _threeStar;
    public static int _maxScore, _finalScore;

    [SerializeField]
    GameObject[] __grassTiles;
    private void Start()
    {        
        _maxScore = 1000;
        _oneStar = Mathf.FloorToInt(_maxScore * 0.4f);
        _twoStar = Mathf.FloorToInt(_maxScore * 0.6f);
        _threeStar = Mathf.FloorToInt(_maxScore * 0.9f);
        __grassTiles = GameObject.FindGameObjectsWithTag("Grass");
    }

    public static int _toolCount, _itemCount, _treeCount, _stumpCount, _megaWeedCount, _growingWeedCount, _grassCount, _plantCount;

    public static void ScoreDeductionCount()
    {
        _growingWeedCount = FindObjectOfType<WeedSpawner>().WeedSoilCount;

        _toolCount = GameObject.FindGameObjectsWithTag("Tool").Length;

        _treeCount = GameObject.FindGameObjectsWithTag("Tree").Length;

        _stumpCount = GameObject.FindGameObjectsWithTag("Stump").Length;

        _megaWeedCount = GameObject.FindGameObjectsWithTag("MegaWeed").Length;

        List<GameObject> _items = new List<GameObject>();
        foreach (GameObject Item in GameObject.FindGameObjectsWithTag("Item"))
        {
            _items.Add(Item);
        }
        _itemCount = _items.Count - _growingWeedCount;
     
        List<GameObject> _grass = new List<GameObject>();
        foreach (GameObject Grass in GameObject.FindGameObjectsWithTag("Grass"))
        {
            if (Grass.GetComponent<GrassTile>()._overgrownGrass == true)
            {
                _grass.Add(Grass);
            }
        }
        _grassCount = _grass.Count;

        /*
        _growingWeedCount = FindObjectOfType<WeedSpawner>().WeedSoilCount;

        List<GameObject> _tools = new List<GameObject>();
        foreach (GameObject Tool in GameObject.FindGameObjectsWithTag("Tool"))
        {
            _tools.Add(Tool);
        }
        _toolCount = _tools.Count;

        List<GameObject> _items = new List<GameObject>();
        foreach (GameObject Item in GameObject.FindGameObjectsWithTag("Item"))
        {
            _items.Add(Item);
        }
        _itemCount = _items.Count - _growingWeedCount;

        List<GameObject> _trees = new List<GameObject>();
        foreach (GameObject Tree in GameObject.FindGameObjectsWithTag("Tree"))
        {
            _trees.Add(Tree);
        }
        _treeCount = _trees.Count;

        List<GameObject> _stumps = new List<GameObject>();
        foreach (GameObject Stumps in GameObject.FindGameObjectsWithTag("Stump"))
        {
            _stumps.Add(Stumps);
        }
        _stumpCount = _stumps.Count;

        List<GameObject> _weeds = new List<GameObject>();
        foreach (GameObject Weed in GameObject.FindGameObjectsWithTag("MegaWeed"))
        {
            _weeds.Add(Weed);
        }
        _megaWeedCount = _weeds.Count;

        List<GameObject> _grass = new List<GameObject>();
        foreach (GameObject Grass in GameObject.FindGameObjectsWithTag("Grass"))
        {
            if (Grass.GetComponent<GrassTile>()._overgrownGrass == true)
            {
                _grass.Add(Grass);
            }
        }
        _grassCount = _grass.Count;
        */
    }

    static int _toolDeduction, _itemDeduction, _treeDeduction, _stumpDeduction, _megaWeedDeduction, _growingWeedDeduction, _grassDeduction;

    public static void FinalScore()
    {
        ScoreDeductionCount();
        _toolDeduction = _toolCount * 20;
        _growingWeedDeduction = _growingWeedCount * 20;
        _itemDeduction = _itemCount * 10;
        _treeDeduction = _treeCount * 40;
        _stumpDeduction = _stumpCount * 20;
        _megaWeedDeduction = _megaWeedCount * 50;
        _grassDeduction = _grassCount * 10;
        _finalScore = _maxScore - _toolDeduction - _itemDeduction - _treeDeduction - _stumpDeduction - _megaWeedDeduction - _growingWeedDeduction - _grassDeduction;
        
    }

}
