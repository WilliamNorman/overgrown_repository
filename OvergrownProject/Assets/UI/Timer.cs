﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

    public float timeRemaining;
    float minutes, seconds;
    public Text timer;
    public GameObject timerText;
    public GameObject timerBackground;
    public GameObject endLevelMenu;
    public GameObject oneStar, twoStar, threeStar;
    public GameObject finalScore;
    public Text score;

    private void Start()
    {
        timerBackground.SetActive(true);
        endLevelMenu.SetActive(false);
        Time.timeScale = 1;

        Invoke("IncreaseMusicTempo", timeRemaining - (timeRemaining / 3));
    }

    // Update is called once per frame
    void Update () {

        timeRemaining -= Time.deltaTime;
        minutes = Mathf.Floor(timeRemaining / 60);
        seconds = Mathf.Floor(timeRemaining % 60);
        timer.text = minutes.ToString("0") + ":" + seconds.ToString("00");
        if (timeRemaining <= 0)
        {
            EndLevel();
        }

	}

    void EndLevel()
    {
        Time.timeScale = 0;
        DisableAllAudio();

        endLevelMenu.SetActive(true);
        timerText.SetActive(false);
        timerBackground.SetActive(false);
        Score.FinalScore();
        score.text = Score._finalScore.ToString();
        if (Score._finalScore > Score._oneStar)
        {
            oneStar.SetActive(true);
        }
        if (Score._finalScore > Score._twoStar)
        {
            twoStar.SetActive(true);
        }
        if (Score._finalScore > Score._threeStar)
        {
            threeStar.SetActive(true);
        }
    }

    //Disable all audio and play normal music
    void DisableAllAudio()
    {
        AudioSource[] _audioSources = FindObjectsOfType<AudioSource>();
        foreach(AudioSource audio in _audioSources)
        {
            audio.Stop();
            audio.loop = false;
        }       
    }
}
